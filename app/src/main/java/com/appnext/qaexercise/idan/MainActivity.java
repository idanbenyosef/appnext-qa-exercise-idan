package com.appnext.qaexercise.idan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.view.View;

import com.appnext.ads.AdsError;
import com.appnext.base.Appnext;
import com.appnext.core.ECPM;
import com.appnext.core.callbacks.OnAdLoaded;
import com.appnext.core.callbacks.OnAdOpened;
import com.appnext.core.callbacks.OnAdClicked;
import com.appnext.core.callbacks.OnAdClosed;
import com.appnext.core.callbacks.OnAdError;

import com.appnext.ads.interstitial.Interstitial;
import com.appnext.ads.interstitial.InterstitialConfig;
import com.appnext.core.callbacks.OnECPMLoaded;

public class MainActivity extends AppCompatActivity {
    private Button mbutton;
    private Interstitial interstitial_Ad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Appnext.init(this);

        mbutton = findViewById(R.id.btn);
        mbutton.setText("Next Level");
        mbutton.setEnabled(true);
        mbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showIntertial();

            }
        });
        InterstitialConfig config = new InterstitialConfig();
        config.setButtonColor("#783562");
        config.setSkipText("QA exercise");
        config.backButtonCanClose = true;
        interstitial_Ad = new Interstitial(this, "24233eba-345a-4128-a6af-48e14d9dd858");

        interstitial_Ad.loadAd();

        // Get callback for ad clicked
        interstitial_Ad.setOnAdClickedCallback(new OnAdClicked() {
            @Override
            public void adClicked() {

            }
        });
        // Get callback for ad loaded
        interstitial_Ad.setOnAdLoadedCallback(new OnAdLoaded() {
            @Override
            public void adLoaded(String bannerID) {
                interstitial_Ad.showAd();

            }
        });
        // Get callback for ad opened
        interstitial_Ad.setOnAdOpenedCallback(new OnAdOpened() {
            @Override
            public void adOpened() {

            }
        });
        // Get callback for ad closed
        interstitial_Ad.setOnAdClosedCallback(new OnAdClosed() {
            @Override
            public void onAdClosed() {

            }
        });
        // Get callback for ad error
        interstitial_Ad.setOnAdErrorCallback(new OnAdError() {
            @Override
            public void adError(String error) {
                switch (error) {
                    case AdsError.NO_ADS:
                        Log.v("appnext", "no ads");
                        break;
                    case AdsError.CONNECTION_ERROR:
                        Log.v("appnext", "connection problem");
                        break;
                    case AdsError.SLOW_CONNECTION:
                        Log.v("appnext", "slow connection");
                        break;
                    case AdsError.AD_NOT_READY:
                        Log.v("appnext", "ad not ready");
                        break;
                    case AdsError.INTERNAL_ERROR:
                        Log.v("appnext", "internal error");
                        break;
                    case AdsError.NO_MARKET:
                        Log.v("appnext", "no market");
                        break;
                    case AdsError.TIMEOUT:
                        Log.v("appnext", "timeout");
                        break;
                    default:
                        Log.v("appnext", "other error");
                        break;
                }
            }
        });
        interstitial_Ad.getECPM(new OnECPMLoaded() {
            @Override
            public void ecpm(ECPM ecpm) {
                Log.v("ecpm", "ecpm: " + ecpm.getEcpm() + ", ppr: " + ecpm.getPpr(
                ) + ", banner ID: " + ecpm.getBanner());
                float appnextEcpm = ecpm.getEcpm();
                if (appnextEcpm > 0.05) {
                    interstitial_Ad.loadAd();
                } else {
                    Log.v("ecpm", "Interstitial ad was not ready to be shown.");
                }
            }

            @Override
            public void error(String error) {
                Log.v("ecpm", "error: " + error);
            }
        });
    }

    public void showIntertial() {
        if (interstitial_Ad.isAdLoaded()) {
            interstitial_Ad.showAd();
        } else {
            Log.v("appnext", "Interstitial ad was not ready to be shown.");
        }
    }
}